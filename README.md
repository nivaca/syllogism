This package provides a simple way for neatly typesetting syllogisms and
syllogistic-like arguments, composed of two premisses and a conclusion. 
It is fully configurable, allowing the user to tweak the various distances, 
line widths, and other options.

Author: Nicolas Vaughan
License: LaTeX Project Public License
